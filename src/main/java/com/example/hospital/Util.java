package com.example.hospital;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Util {
    private static final String DB_DRIVE = "com.mysql.cj.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:8889/hospital";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "root";

    public static  Connection getConnection(){

        Connection connection = null;
        try {
            Class.forName(DB_DRIVE);
            connection= DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
            System.out.println("Connection OK!!!");


        }catch (ClassNotFoundException | SQLException e){
            e.printStackTrace();
            System.out.println("Connection ERROR!");
        }
        return connection;
    }

//    public static void main(String[] args) {
//        Util util = new Util();
//        Util.getConnection();
//    }
}
