package com.example.hospital.dao;


import com.example.hospital.model.PatientCard;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface PatientCardDAO {
    boolean add(PatientCard patientCard) throws SQLException;
    PatientCard getById(int idCard) throws SQLException;
    boolean update(PatientCard patientCard) throws SQLException;
    String delete(int idCard) throws SQLException;
    ArrayList<Map<String, String>> getAllCards(String roles,String role_id,String id)throws SQLException;

}
