package com.example.hospital.dao;

import com.example.hospital.model.PatientStatus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface PatientStatusDAO {
    boolean add(PatientStatus patientStatus) throws SQLException;
//    void update(PatientStatus patientStatus);
//    void delete(PatientStatus patientStatus);
     ArrayList<Map<String, String>> allStatus ();
}
