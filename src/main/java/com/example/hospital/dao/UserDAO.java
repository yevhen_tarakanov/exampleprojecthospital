package com.example.hospital.dao;

import com.example.hospital.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface UserDAO {

    boolean add(User user) throws SQLException;

    User getById(int id) throws SQLException;

    boolean update(User user) throws SQLException;

    String delete(int id) throws SQLException;

     ArrayList<Map<String, String>> getAllPatient(String role, String login);

     ArrayList<Map<String, String>> getAllDoctor(String roles, String login);

     boolean getAllUsers(String login, String password) throws SQLException;

     User getRoleUser(String login, String password) throws SQLException;

     ArrayList<Map<String, String>> getAmountPatient();

     ArrayList<Map<String, String>> getDoctor();
}





