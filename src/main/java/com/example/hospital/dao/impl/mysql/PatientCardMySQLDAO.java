package com.example.hospital.dao.impl.mysql;

import com.example.hospital.dao.PatientCardDAO;
import com.example.hospital.model.PatientCard;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.example.hospital.Util.getConnection;
public class PatientCardMySQLDAO implements PatientCardDAO {
    @Override
    public boolean add(PatientCard patientCard) throws SQLException {

        String sql = "INSERT INTO patient_card(user_id,doctor_id,diagnosis,treatment,drug,operations,status_id)  VALUES(?,?,?,?,?,?,?)";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setInt(1, patientCard.getUser_id());
            preparedStatement.setInt(2, patientCard.getDoctor_id());
            preparedStatement.setString(3, patientCard.getDiagnosis());
            preparedStatement.setString(4, patientCard.getTreatment());
            preparedStatement.setString(5,patientCard.getDrug());
            preparedStatement.setString(6,patientCard.getOperations());
            preparedStatement.setInt(7, patientCard.getStatus_id());

            preparedStatement.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public PatientCard getById(int id)  {
        PatientCard patientCard= new PatientCard();
        String sql = "SELECT * FROM patient_card WHERE idCard=?";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                patientCard.setIdCard(rs.getInt("idCard"));
                patientCard.setUser_id(rs.getInt("user_id"));
                patientCard.setDoctor_id(rs.getInt("doctor_id"));
                patientCard.setDiagnosis(rs.getString("diagnosis"));
                patientCard.setTreatment(rs.getString("treatment"));
                patientCard.setDrug(rs.getString("drug"));
                patientCard.setOperations(rs.getString("operations"));
                patientCard.setStatus_id(rs.getInt("status_id"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return patientCard;
    }

    @Override
    public boolean update(PatientCard patientCard) throws SQLException {

        String sql = "UPDATE patient_card SET doctor_id = ?, diagnosis = ?, treatment = ?,drug = ?,operations = ?, status_id = ? WHERE idCard = ?";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(7, String.valueOf(patientCard.getIdCard()));

            preparedStatement.setString(1, String.valueOf(patientCard.getDoctor_id()));
            preparedStatement.setString(2, patientCard.getDiagnosis());
            preparedStatement.setString(3, patientCard.getTreatment());
            preparedStatement.setString(4, patientCard.getDrug());
            preparedStatement.setString(5, patientCard.getOperations());
            preparedStatement.setString(6, String.valueOf(patientCard.getStatus_id()));

            preparedStatement.executeUpdate();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String delete(int idCard) throws SQLException {
        String sql = "DELETE FROM patient_card where idCard=?";
        try  (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)){

            preparedStatement.setInt(1, idCard);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        return null;
    }



    @Override

        public ArrayList<Map<String, String>> getAllCards(String roles,String role_id,String id) {

            ArrayList<Map<String, String>> listCard = new ArrayList<>();

        String sqlPatientCardUser = " SELECT patient_card.idCard , patient_card.diagnosis , patient_card.treatment, patient_card.drug , patient_card.operations , u.id, u.lastname , us.lastname , us.firstname , us.patronymic , us.role_id , r.role_name , r.positions , ps.status FROM patient_card JOIN user u ON patient_card.user_id = u.id JOIN user us ON patient_card.doctor_id = us.id JOIN role r ON r.role_id = us.role_id JOIN patient_status ps ON patient_card.status_id = ps.id"; //WHERE idCard=?";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sqlPatientCardUser)) {
//            preparedStatement.setInt(1,idCards);
            ResultSet rsUser = preparedStatement.executeQuery();

            while (rsUser.next()) {
                String idCard = rsUser.getString("patient_card.idCard");

                String idBD = rsUser.getString("u.id");

                String diagnosis = rsUser.getString("patient_card.diagnosis");
                String treatment = rsUser.getString("patient_card.treatment");
                String drug = rsUser.getString("patient_card.drug");
                String operations = rsUser.getString("patient_card.operations");
                String lastname = rsUser.getString("u.lastname");
                String lastnameDoc = rsUser.getString("us.lastname");
                String firstnameDoc = rsUser.getString("us.firstname");
                String patronymicDoc = rsUser.getString("us.patronymic");
                String role_idDoc = rsUser.getString("us.role_id");
                String role_name = rsUser.getString("r.role_name");
                String role_positions = rsUser.getString("r.positions");
                String status = rsUser.getString("ps.status");


                switch (roles) {
                    case "DOC":
                        if (Objects.equals(role_id, role_idDoc)) {
                            Map<String, String> mapCard = new HashMap<>();
                            mapCard.put("idCard", idCard);
                            mapCard.put("id", idBD);
                            mapCard.put("diagnosis", diagnosis);
                            mapCard.put("treatment", treatment);
                            mapCard.put("lastname", lastname);
                            mapCard.put("drug", drug);
                            mapCard.put("operations", operations);
                            mapCard.put("firstnameDoc", firstnameDoc);
                            mapCard.put("lastnameDoc", lastnameDoc);
                            mapCard.put("role_idDoc", role_idDoc);
                            mapCard.put("role_name", role_name);
                            mapCard.put("role_positions", role_positions);
                            mapCard.put("patronymicDoc", patronymicDoc);
                            mapCard.put("status", status);

                            listCard.add(mapCard);

                        }
                        break;
                    case "USER":
                        if (Objects.equals(id, idBD)) {
                            Map<String, String> mapCard = new HashMap<>();
                            mapCard.put("idCard", idCard);
                            mapCard.put("id", idBD);
                            mapCard.put("diagnosis", diagnosis);
                            mapCard.put("treatment", treatment);
                            mapCard.put("lastname", lastname);
                            mapCard.put("drug", drug);
                            mapCard.put("operations", operations);
                            mapCard.put("firstnameDoc", firstnameDoc);
                            mapCard.put("lastnameDoc", lastnameDoc);
                            mapCard.put("role_idDoc", role_idDoc);
                            mapCard.put("role_name", role_name);
                            mapCard.put("role_positions", role_positions);
                            mapCard.put("patronymicDoc", patronymicDoc);
                            mapCard.put("status", status);

                            listCard.add(mapCard);
                        }
                        break;
                    default:
                        Map<String, String> mapCard = new HashMap<>();
                        mapCard.put("idCard", idCard);
                        mapCard.put("id", idBD);
                        mapCard.put("diagnosis", diagnosis);
                        mapCard.put("treatment", treatment);
                        mapCard.put("lastname", lastname);
                        mapCard.put("drug", drug);
                        mapCard.put("operations", operations);
                        mapCard.put("firstnameDoc", firstnameDoc);
                        mapCard.put("lastnameDoc", lastnameDoc);
                        mapCard.put("role_idDoc", role_idDoc);
                        mapCard.put("role_name", role_name);
                        mapCard.put("role_positions", role_positions);
                        mapCard.put("patronymicDoc", patronymicDoc);
                        mapCard.put("status", status);

                        listCard.add(mapCard);
                        break;

                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return listCard;
    }



}
