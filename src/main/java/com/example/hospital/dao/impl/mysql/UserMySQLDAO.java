package com.example.hospital.dao.impl.mysql;

import com.example.hospital.dao.UserDAO;
import com.example.hospital.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.example.hospital.Util.getConnection;

public class UserMySQLDAO implements UserDAO {


    @Override
    public boolean add(User user) throws SQLException {
        String sql = "INSERT INTO user(firstname,lastname,login,password,gender,birthday,patronymic,role_id,roles)  VALUES(?,?,?,?,?,?,?,?,?)";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, user.getFirstname());
            preparedStatement.setString(2, user.getLastname());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getGender());
            preparedStatement.setDate(6, user.getBirthday());
            preparedStatement.setString(7, user.getpatronymic());
            preparedStatement.setString(8, user.getRole_id());
            preparedStatement.setString(9, user.getRole());

            preparedStatement.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public User getById(int id) {
        User user = new User();
        String sql = "SELECT * FROM user WHERE id=?";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setFirstname(rs.getString("firstname"));
                user.setLastname(rs.getString("lastname"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setpatronymic(rs.getString("patronymic"));
                user.setGender(rs.getString("gender"));
                user.setBirthday(rs.getDate("birthday"));
                user.setRole_id(rs.getString("role_id"));
                user.setRole(rs.getString("roles"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return user;
    }


    @Override
    public boolean update(User user) throws SQLException {

        String sql = "UPDATE user SET firstname = ?, lastname = ?, login = ?,password = ?,gender = ?,birthday = ?,patronymic = ? WHERE id = ?";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(8, String.valueOf(user.getId()));

            preparedStatement.setString(1, user.getFirstname());
            preparedStatement.setString(2, user.getLastname());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getGender());
            preparedStatement.setString(6, String.valueOf(user.getBirthday()));
            preparedStatement.setString(7, user.getpatronymic());

            preparedStatement.executeUpdate();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String delete(int id) throws SQLException {
        String sql = "DELETE FROM user where id=?";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setInt(1, id);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        }

        return null;
    }

    @Override
    public boolean getAllUsers(String login, String password)  {
        String sql = "SELECT * FROM user";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                if (login.equals(rs.getString("login")) && password.equals(rs.getString("password"))) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public User getRoleUser(String login, String password)  {
        User user = new User();
        String sql = "SELECT * FROM user WHERE login=? AND password=?";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setFirstname(rs.getString("firstname"));
                user.setLastname(rs.getString("lastname"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setpatronymic(rs.getString("patronymic"));
                user.setGender(rs.getString("gender"));
                user.setBirthday(rs.getDate("birthday"));
                user.setRole_id(rs.getString("role_id"));
                user.setRole(rs.getString("roles"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return user;
    }

    @Override
    public ArrayList<Map<String, String>> getAllPatient(String roles, String login) {
        ArrayList<Map<String, String>> listDoctor = new ArrayList<>();
        String sqlDoctor = " SELECT * FROM user u JOIN role r ON u.role_id = r.role_id";

        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sqlDoctor)) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String id = rs.getString("id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String birthday = rs.getString("birthday");
                String logins = rs.getString("login");
                String password = rs.getString("password");
                String gender = rs.getString("gender");
                String role_id = rs.getString("role_id");
                String patronymic = rs.getString("patronymic");
                String role = rs.getString("roles");
                String role_id1 = rs.getString("role_id");
                String role_name = rs.getString("role_name");
                String positions = rs.getString("positions");

                switch (roles) {
                    case "ADMIN":
                        if (Objects.equals(role, "USER")) {
                            Map<String, String> mapRole = new HashMap<>();
                            mapRole.put("id", id);
                            mapRole.put("firstname", firstname);
                            mapRole.put("lastname", lastname);
                            mapRole.put("birthday", birthday);
                            mapRole.put("login", logins);
                            mapRole.put("password", password);
                            mapRole.put("gender", gender);
                            mapRole.put("role_id", role_id);
                            mapRole.put("patronymic", patronymic);
                            mapRole.put("role_id1", role_id1);
                            mapRole.put("role_name", role_name);
                            mapRole.put("positions", positions);

                            listDoctor.add(mapRole);
                        }
                        break;

                    case "DOC":
                        if (!Objects.equals(roles, role) && !Objects.equals(role, "NURSE")) {
                            Map<String, String> mapRole = new HashMap<>();
                            mapRole.put("id", id);
                            mapRole.put("firstname", firstname);
                            mapRole.put("lastname", lastname);
                            mapRole.put("birthday", birthday);
                            mapRole.put("login", logins);
                            mapRole.put("password", password);
                            mapRole.put("gender", gender);
                            mapRole.put("role_id", role_id);
                            mapRole.put("patronymic", patronymic);
                            mapRole.put("role_id1", role_id1);
                            mapRole.put("role_name", role_name);
                            mapRole.put("positions", positions);

                            listDoctor.add(mapRole);
                        }
                        break;

                    case "NURSE":
                        if (!Objects.equals(roles, role) && !Objects.equals(role, "DOC") && !Objects.equals(role, "NURSE")) {
                            Map<String, String> mapRole = new HashMap<>();
                            mapRole.put("id", id);
                            mapRole.put("firstname", firstname);
                            mapRole.put("lastname", lastname);
                            mapRole.put("birthday", birthday);
                            mapRole.put("login", logins);
                            mapRole.put("password", password);
                            mapRole.put("gender", gender);
                            mapRole.put("role_id", role_id);
                            mapRole.put("patronymic", patronymic);
                            mapRole.put("role_id1", role_id1);
                            mapRole.put("role_name", role_name);
                            mapRole.put("positions", positions);

                            listDoctor.add(mapRole);
                        }
                        break;

                    case "USER":
                        if (Objects.equals(roles, role) && Objects.equals(login, logins) && !Objects.equals(role, "NURSE")) {
                            Map<String, String> mapRole = new HashMap<>();
                            mapRole.put("id", id);
                            mapRole.put("firstname", firstname);
                            mapRole.put("lastname", lastname);
                            mapRole.put("birthday", birthday);
                            mapRole.put("login", logins);
                            mapRole.put("password", password);
                            mapRole.put("gender", gender);
                            mapRole.put("role_id", role_id);
                            mapRole.put("patronymic", patronymic);
                            mapRole.put("role_id1", role_id1);
                            mapRole.put("role_name", role_name);
                            mapRole.put("positions", positions);

                            listDoctor.add(mapRole);
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return listDoctor;
    }

    @Override
    public ArrayList<Map<String, String>> getAllDoctor(String roles, String login) {
        ArrayList<Map<String, String>> listDoctor = new ArrayList<>();
        String sqlDoctor = " SELECT * FROM user u JOIN role r ON u.role_id = r.role_id";

        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sqlDoctor)) {
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String id = rs.getString("id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String birthday = rs.getString("birthday");
                String logins = rs.getString("login");
                String password = rs.getString("password");
                String gender = rs.getString("gender");
                String role_id = rs.getString("role_id");
                String patronymic = rs.getString("patronymic");
                String role = rs.getString("roles");
                String role_id1 = rs.getString("role_id");
                String role_name = rs.getString("role_name");
                String positions = rs.getString("positions");

                switch (roles) {
                    case "ADMIN":
                        if (!Objects.equals(role, "USER") && !Objects.equals(role, "ADMIN")) {
                            Map<String, String> mapRole = new HashMap<>();
                            mapRole.put("id", id);
                            mapRole.put("firstname", firstname);
                            mapRole.put("lastname", lastname);
                            mapRole.put("birthday", birthday);
                            mapRole.put("login", logins);
                            mapRole.put("password", password);
                            mapRole.put("gender", gender);
                            mapRole.put("role_id", role_id);
                            mapRole.put("patronymic", patronymic);
                            mapRole.put("role_id1", role_id1);
                            mapRole.put("role_name", role_name);
                            mapRole.put("positions", positions);

                            listDoctor.add(mapRole);
                        }
                        break;

                    case "DOC":

                        if (Objects.equals(roles, role) && Objects.equals(login, logins) && !Objects.equals(role, "NURSE")) {
                            Map<String, String> mapRole = new HashMap<>();
                            mapRole.put("id", id);
                            mapRole.put("firstname", firstname);
                            mapRole.put("lastname", lastname);
                            mapRole.put("birthday", birthday);
                            mapRole.put("login", logins);
                            mapRole.put("password", password);
                            mapRole.put("gender", gender);
                            mapRole.put("role_id", role_id);
                            mapRole.put("patronymic", patronymic);
                            mapRole.put("role_id1", role_id1);
                            mapRole.put("role_name", role_name);
                            mapRole.put("positions", positions);

                            listDoctor.add(mapRole);
                        }
                        break;

                    case "NURSE":
                        if (Objects.equals(roles, role) && Objects.equals(login, logins)) {
                            Map<String, String> mapRole = new HashMap<>();
                            mapRole.put("id", id);
                            mapRole.put("firstname", firstname);
                            mapRole.put("lastname", lastname);
                            mapRole.put("birthday", birthday);
                            mapRole.put("login", logins);
                            mapRole.put("password", password);
                            mapRole.put("gender", gender);
                            mapRole.put("role_id", role_id);
                            mapRole.put("patronymic", patronymic);
                            mapRole.put("role_id1", role_id1);
                            mapRole.put("role_name", role_name);
                            mapRole.put("positions", positions);

                            listDoctor.add(mapRole);
                        }
                        break;

                    case "USER":
                        if (!Objects.equals(roles, role) && !Objects.equals(role, "NURSE")) {
                            Map<String, String> mapRole = new HashMap<>();
                            mapRole.put("id", id);
                            mapRole.put("firstname", firstname);
                            mapRole.put("lastname", lastname);
                            mapRole.put("birthday", birthday);
                            mapRole.put("login", logins);
                            mapRole.put("password", password);
                            mapRole.put("gender", gender);
                            mapRole.put("role_id", role_id);
                            mapRole.put("patronymic", patronymic);
                            mapRole.put("role_id1", role_id1);
                            mapRole.put("role_name", role_name);
                            mapRole.put("positions", positions);

                            listDoctor.add(mapRole);
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listDoctor;
    }


    @Override
    public ArrayList<Map<String, String>> getAmountPatient() {
        String roleD = "DOC";
        String roleN = "NURSE";
        ArrayList<Object> listDoc = new ArrayList<>();
        try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
            String sqlRole = " SELECT * FROM user u JOIN role r ON u.role_id = r.role_id";
            ResultSet rs = statement.executeQuery(sqlRole);
            while (rs.next()) {
                String id = rs.getString("id");
                String roles = rs.getString("roles");
                if (Objects.equals(roleD, roles) || Objects.equals(roleN, roles)) {
                    listDoc.add(id);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<Map<String, String>> listAmountPatient = new ArrayList<>();
        for (Object stringMap : listDoc) {
            int sum = 0;
            try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
                String sqlAmount = " SELECT * FROM user u JOIN patient_card p ON u.id = p.doctor_id";
                ResultSet rs = statement.executeQuery(sqlAmount);
                while (rs.next()) {
                    String doctor_id = rs.getString("doctor_id");
                    if (Objects.equals(stringMap, doctor_id)) {
                        sum = sum + 1;
                    }
                }
                Map<String, String> map = new HashMap<>();
                map.put("id", String.valueOf(stringMap));
                map.put("sum", String.valueOf(sum));
                listAmountPatient.add(map);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return listAmountPatient;
    }

    @Override
    public ArrayList<Map<String, String>> getDoctor() {
        String roles = "DOC";

        ArrayList<Map<String, String>> listDoctor = new ArrayList<>();
        String sqlRole = " SELECT * FROM user u JOIN role r ON u.role_id = r.role_id";
        try (
                Connection connection = getConnection();
                Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(sqlRole);
            while (rs.next()) {
                String id = rs.getString("id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String birthday = rs.getString("birthday");
                String login = rs.getString("login");
                String password = rs.getString("password");
                String gender = rs.getString("gender");
                String role_id = rs.getString("role_id");
                String patronymic = rs.getString("patronymic");
                String role_id1 = rs.getString("role_id");
                String role_name = rs.getString("role_name");
                String positions = rs.getString("positions");
                String role = rs.getString("roles");
                if (Objects.equals(role, roles)) {
                    Map<String, String> mapRole = new HashMap<>();
                    mapRole.put("id", id);
                    mapRole.put("firstname", firstname);
                    mapRole.put("lastname", lastname);
                    mapRole.put("birthday", birthday);
                    mapRole.put("login", login);
                    mapRole.put("password", password);
                    mapRole.put("gender", gender);
                    mapRole.put("role_id", role_id);
                    mapRole.put("patronymic", patronymic);
                    mapRole.put("role_id1", role_id1);
                    mapRole.put("role_name", role_name);
                    mapRole.put("positions", positions);
                    listDoctor.add(mapRole);
                }
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        return listDoctor;
    }

}