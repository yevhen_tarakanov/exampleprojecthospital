package com.example.hospital.dao.impl.mysql;

import com.example.hospital.dao.RoleDAO;
import com.example.hospital.model.Role;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.hospital.Util.getConnection;
public class RoleMySQLDAO implements RoleDAO {




    @Override
        public boolean add(Role role) throws SQLException {
            String sql = "INSERT INTO role (role_name,positions)  VALUES(?,?)";
            try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

                preparedStatement.setString(1, role.getRole_name());
                preparedStatement.setString(2, role.getPosition());
                preparedStatement.executeUpdate();
                return true;

            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }


//    @Override
//    public void update(Role role) {
//
//    }
//
//    @Override
//    public void delete(Role role) {
//
//    }
    @Override
            public ArrayList<Map<String,String>> allRole () {
        ArrayList<Map<String, String>> listRole = new ArrayList<>(); // Создание коллекции списка для нанесения карты
        String sql = "SELECT * FROM role";
        try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                String role_id = rs.getString("role_id");
                String role_name = rs.getString("role_name");
                String positions = rs.getString("positions");
                Map<String, String> map = new HashMap<>();
                map.put("role_id", role_id);
                map.put("role_name", role_name);
                map.put("positions", positions);
                listRole.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    return listRole;
    }
}
