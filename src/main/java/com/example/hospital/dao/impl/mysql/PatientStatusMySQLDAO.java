package com.example.hospital.dao.impl.mysql;

import com.example.hospital.dao.PatientStatusDAO;
import com.example.hospital.model.PatientStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.hospital.Util.getConnection;
public class PatientStatusMySQLDAO implements PatientStatusDAO {
    @Override
    public boolean add(PatientStatus patientStatus) throws SQLException{

        String sql = "INSERT INTO patient_status(status)  VALUES(?)";
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, patientStatus.getStatus());

            preparedStatement.executeUpdate();
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


//    @Override
//    public void update(PatientStatus patientStatus) {
//
//    }
//
//    @Override
//    public void delete(PatientStatus patientStatus) {
//
//    }

    @Override
            public ArrayList<Map<String, String>> allStatus () {
        ArrayList<Map<String, String>> listStatus = new ArrayList<>(); // Создание коллекции списка для нанесения карты
        String sqlStatus = "SELECT * FROM patient_status";
        try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(sqlStatus);
            while (rs.next()) {
                String id = rs.getString("id");
                String status = rs.getString("status");
                Map<String, String> mapStatus = new HashMap<>();
                mapStatus.put("id", id);
                mapStatus.put("status", status);
                listStatus.add(mapStatus);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listStatus;
    }
}
