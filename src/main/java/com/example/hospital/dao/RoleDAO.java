package com.example.hospital.dao;

import com.example.hospital.model.Role;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public interface RoleDAO {
    boolean add(Role role) throws SQLException;
//    void update(Role role);//not working
//    void delete(Role role);//not working
    ArrayList<Map<String,String>> allRole ();
}
