package com.example.hospital.listener;

import com.example.hospital.dao.PatientCardDAO;
import com.example.hospital.dao.PatientStatusDAO;
import com.example.hospital.dao.RoleDAO;
import com.example.hospital.dao.UserDAO;
import com.example.hospital.dao.impl.mysql.PatientCardMySQLDAO;
import com.example.hospital.dao.impl.mysql.PatientStatusMySQLDAO;
import com.example.hospital.dao.impl.mysql.RoleMySQLDAO;
import com.example.hospital.dao.impl.mysql.UserMySQLDAO;
import com.example.hospital.service.PatientCardService;
import com.example.hospital.service.PatientStatusService;
import com.example.hospital.service.RoleService;
import com.example.hospital.service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListner implements ServletContextListener {



    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContextListener.super.contextInitialized(sce);
        ServletContext servletContext = sce.getServletContext();

        UserDAO userDao = new UserMySQLDAO();
        UserService userService = new UserService(userDao);
        servletContext.setAttribute("userService", userService);

        RoleDAO roleDAO = new RoleMySQLDAO();
        RoleService roleService = new RoleService(roleDAO);
        servletContext.setAttribute("roleService", roleService);

        PatientStatusDAO patientStatusDAO = new PatientStatusMySQLDAO();
        PatientStatusService patientStatusService = new PatientStatusService(patientStatusDAO);
        servletContext.setAttribute("patientStatusService", patientStatusService);

        PatientCardDAO patientCardDAO = new PatientCardMySQLDAO();
        PatientCardService patientCardService = new PatientCardService(patientCardDAO);
        servletContext.setAttribute("patientCardService", patientCardService);

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContextListener.super.contextDestroyed(sce);

    }


}
