package com.example.hospital.service;

import com.example.hospital.dao.RoleDAO;
import com.example.hospital.model.Role;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class RoleService {

    private final RoleDAO roleDAO ;


    public RoleService(RoleDAO roleDAO) {
        this.roleDAO = roleDAO;
    }

    public Role add(String role_name,String positions) {
        try {
            Role role = new Role(role_name, positions);
            boolean roleCreated = roleDAO.add(role);
            return (roleCreated) ? role : null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public ArrayList<Map<String,String>> allRole (){
        return roleDAO.allRole();
    }
}
