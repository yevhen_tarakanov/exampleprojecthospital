package com.example.hospital.service;

import com.example.hospital.dao.PatientStatusDAO;
import com.example.hospital.model.PatientStatus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class PatientStatusService {
    private final PatientStatusDAO patientStatusDAO;

    public PatientStatusService(PatientStatusDAO patientStatusDAO) {
        this.patientStatusDAO = patientStatusDAO;

    }

    public PatientStatus add(String status) {
        try {
            PatientStatus patientStatus = new PatientStatus(status);
            boolean patientStatusCreated = patientStatusDAO.add(patientStatus);
            return (patientStatusCreated) ? patientStatus : null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Map<String, String>> allStatus (){
        return patientStatusDAO.allStatus();
    }
}
