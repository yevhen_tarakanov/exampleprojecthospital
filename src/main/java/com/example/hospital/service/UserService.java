package com.example.hospital.service;

import com.example.hospital.dao.UserDAO;
import com.example.hospital.model.User;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class UserService  {

    private final UserDAO userDAO ;

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }




    public User add(String firstname, String lastname, String login, String password, String gender, Date birthday, String patronymic, String role_id, String roles) {
        try {
            User user = new User(birthday, firstname, lastname, login, password, gender, patronymic, role_id, roles);
            boolean userCreated = userDAO.add(user);
            return (userCreated) ? user : null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean update(User user) {
        try {
            return userDAO.update(user);
        } catch (SQLException e) {
            return false;
        }
    }

    public User getById(int id) {
        try {
            return userDAO.getById(id);
        } catch (SQLException e) {
            return null;
        }
    }


    public String delete(int id) {
        try {
            return userDAO.delete(id);
        } catch (SQLException e) {
            return null;
        }
    }


    public ArrayList<Map<String, String>> getAllPatient(String role, String login){
        return userDAO.getAllPatient(role,login);
    }
    public ArrayList<Map<String, String>> getAllDoctor(String roles,String login){
    return userDAO.getAllDoctor(roles,login);
    }

    public boolean getAllUsers(String login, String password)  {
        try {
            return userDAO.getAllUsers(login,password);
        } catch (SQLException e) {
            return false;
        }
    }

    public User getRoleUser(String login, String password)throws SQLException{
        try {
            return userDAO.getRoleUser(login, password);
        } catch (SQLException e) {
            return null;
        }
    }

    public ArrayList<Map<String,String>> getAmountPatient(){
        return userDAO.getAmountPatient();
    }
    public ArrayList<Map<String, String>> getDoctor(){
        return  userDAO.getDoctor();
    }






}
