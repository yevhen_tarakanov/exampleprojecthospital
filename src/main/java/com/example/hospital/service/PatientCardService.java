package com.example.hospital.service;

import com.example.hospital.dao.PatientCardDAO;
import com.example.hospital.model.PatientCard;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class PatientCardService {

    private final PatientCardDAO patientCardDAO ;

    public PatientCardService(PatientCardDAO patientCardDAO) {
        this.patientCardDAO = patientCardDAO;
    }

    public PatientCard add(String user_id, String doctor_id, String diagnosis, String treatment, String drug, String operations, String status_id) {
        try {
            PatientCard patientCard = new PatientCard(user_id, doctor_id, diagnosis, treatment,drug, operations, status_id);
            boolean patientCardCreated = patientCardDAO.add(patientCard);
            return (patientCardCreated) ? patientCard : null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    public PatientCard getById(int idCard){
        try {
            return patientCardDAO.getById(idCard);
        } catch (SQLException e) {
            return null;
        }
    }
    public boolean update(PatientCard patientCard) {
        try {
            return patientCardDAO.update(patientCard);
        } catch (SQLException e) {
            return false;
        }
    }
    public String delete(int idCard) {
        try {
            return patientCardDAO.delete(idCard);
        } catch (SQLException e) {
            return null;
        }
    }

    public ArrayList<Map<String, String>> getAllCards(String roles, String role_id, String id){
        try {
            return patientCardDAO.getAllCards(roles,role_id,id);
        } catch (SQLException e) {
            return null;
        }
    }

}
