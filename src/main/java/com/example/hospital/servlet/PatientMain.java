package com.example.hospital.servlet;

import com.example.hospital.service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "patient",value = "/patient")
public class PatientMain extends HttpServlet {

    private UserService userService ;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext =  getServletContext();
        userService = (UserService) servletContext.getAttribute("userService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");


        final HttpSession session = request.getSession();
        String roles =(String) session.getAttribute("roles");
        String login = (String) session.getAttribute("login");

        request.setAttribute("userlist", userService.getAllPatient(roles,login));

        request.getRequestDispatcher("/patient/patient-main.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
    }

}
