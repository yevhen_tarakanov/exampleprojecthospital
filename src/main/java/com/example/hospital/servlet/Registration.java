package com.example.hospital.servlet;


import com.example.hospital.model.User;
import com.example.hospital.service.RoleService;
import com.example.hospital.service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;

@WebServlet(name = "registration", value = "/registration")
public class Registration extends HttpServlet {

    private UserService userService ;
    private RoleService roleService;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext =  getServletContext();
        userService = (UserService) servletContext.getAttribute("userService");
        roleService = (RoleService) servletContext.getAttribute("roleService");
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute ("role", roleService.allRole());

        request.getRequestDispatcher("/registration.jsp").forward(request, response);
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String gender = req.getParameter("gender");
        String birthday = req.getParameter("birthday");
        String  patronymic = req.getParameter("patronymic");
        String role_id = req.getParameter("role_id");
        String roles = req.getParameter("roles");

        MessageDigest md5 ;
        StringBuilder builder = new StringBuilder();
        try {
             md5 = MessageDigest.getInstance("MD5");


            byte[] bytes = md5.digest(password.getBytes());

            for (byte b : bytes) {
                builder.append(String.format("%02X",b));
            }
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        password= String.valueOf(builder);


        if (firstname.equals("") && lastname.equals("") && login.equals("") && password.equals("") && patronymic.equals("") && role_id.equals("") && roles.equals("") && birthday.equals("")) {
            throw new ServletException();
        }
            else {
                User user = userService.add(firstname, lastname, login, password, gender, Date.valueOf(birthday), patronymic, role_id, roles);

                if (user != null) {
                    req.getSession().setAttribute("User", user);

                    req.getSession().removeAttribute("User");
                    req.getRequestDispatcher("authorization.jsp").forward(req, resp);
                } else {
                    resp.sendRedirect("errors.jsp");
                }
            }

    }

}


