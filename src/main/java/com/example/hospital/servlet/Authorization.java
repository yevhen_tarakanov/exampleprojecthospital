package com.example.hospital.servlet;

import com.example.hospital.service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import static java.util.Objects.nonNull;

@WebServlet(name = "authorization", value = "/authorization")
public class Authorization extends HttpServlet {

    private UserService userService ;


    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext =  getServletContext();
       userService = (UserService) servletContext.getAttribute("userService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        req.getRequestDispatcher("/authorization.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        String login = req.getParameter("logins");
        String password = req.getParameter("passwords");

        MessageDigest md5 ;
        StringBuilder builder = new StringBuilder();
        try {
            md5 = MessageDigest.getInstance("MD5");

            byte[] bytes = md5.digest(password.getBytes());
            for (byte b : bytes) {
                builder.append(String.format("%02X",b));
            }
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        password= String.valueOf(builder);

         HttpSession session = req.getSession();

        //Logged user.
        if (nonNull(session) &&
                nonNull(session.getAttribute("login")) &&
                nonNull(session.getAttribute("password"))) {

            final String roles = (String) session.getAttribute("roles");
            moveToMenu(req, resp, roles);
        }

        else if (userService.getAllUsers(login, password)==true) {

            try {
                final String roles = userService.getRoleUser(login, password).getRole();
                final String role_id = userService.getRoleUser(login, password).getRole_id();
                final String id = String.valueOf(userService.getRoleUser(login, password).getId());

                assert session != null;
                session.setAttribute("login", login);
                session.setAttribute("roles", roles);
                session.setAttribute("roleid", role_id);
                session.setAttribute("id", id);

                moveToMenu(req, resp, String.valueOf(roles));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }


                }
        else if (userService.getAllUsers(login, password)==false) {
            final String roles = "";
            assert session != null;
            session.removeAttribute("login");
            session.removeAttribute("roles");
            moveToMenu(req, resp, roles);
        }
    }


    private void moveToMenu(final HttpServletRequest req,
                            final HttpServletResponse resp,
                            final String roles)
            throws  IOException {

        String redirect;

        switch (roles) {
            case "ADMIN":
                redirect = "/admin";
                break;

            case "DOC":
                redirect = "/patient";
                break;

            case "NURSE":
                redirect = "/patient";
                break;

            case "USER":
                redirect = "/doctor";
                break;

            default:
                redirect = "/error";
                break;
        }
        resp.sendRedirect(redirect);
    }


    @Override
    public void destroy() {
    }
}

