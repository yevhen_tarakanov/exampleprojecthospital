package com.example.hospital.servlet;

import com.example.hospital.model.User;
import com.example.hospital.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

public class UserHandler extends HttpServlet {
    private static final String EDIT = "/patient/edit.jsp";
    private static final String USER = "/patient/patient-main.jsp";


    private UserService userService ;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext =  getServletContext();
        userService = (UserService) servletContext.getAttribute("userService");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        String redirect = "";
        String action = request.getParameter("action");
         if (action.equalsIgnoreCase("delete")) {
            String id = request.getParameter("id");
            int uid = Integer.parseInt(id);
             userService.delete(uid);
             redirect = USER;
        }
        else if (action.equalsIgnoreCase("editform")) {

            String userId = request.getParameter("id");
            User user ;
            int id = Integer.parseInt(userId);
                user = userService.getById(id);

            user.setId(id);
            user.setFirstname(user.getFirstname());
            user.setLastname(user.getLastname());
            user.setpatronymic(user.getpatronymic());
            user.setLogin(user.getLogin());
            user.setPassword(user.getPassword());
             System.out.println("1 " + user.getPassword());
            user.setBirthday(user.getBirthday());
            user.setGender(user.getGender());
            request.setAttribute("User", user);
            redirect = EDIT;
        }
        else if (action.equalsIgnoreCase("edit")) {
            String id = request.getParameter("id");
            int uid = Integer.parseInt(id);
            User user = new User();
            user.setId(uid);
            user.setFirstname(request.getParameter("firstname"));
            user.setLastname(request.getParameter("lastname"));
            user.setLogin(request.getParameter("login"));
            user.setPassword(request.getParameter("password"));
             System.out.println("2 " + user.getPassword());
            user.setGender(request.getParameter("gender"));
            user.setBirthday(Date.valueOf(request.getParameter("birthday")));
            user.setpatronymic(request.getParameter("patronymic"));

            userService.update(user);


         }

        RequestDispatcher rd = request.getRequestDispatcher(redirect);
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        doGet(request, response);
    }
}