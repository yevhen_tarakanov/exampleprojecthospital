package com.example.hospital.servlet;

import com.example.hospital.model.Role;
import com.example.hospital.service.RoleService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

    @WebServlet(name = "doctor-add", value = "/doctor/add")
    public class DoctorAdd extends HttpServlet {


        private RoleService roleService ;


        @Override
        public void init() throws ServletException {
            super.init();
            ServletContext servletContext =  getServletContext();
            roleService = (RoleService) servletContext.getAttribute("roleService");
        }


          @Override
          protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
              request.getRequestDispatcher("/doctors/doctor-add.jsp").forward(request, response);
              request.setCharacterEncoding("utf-8");
              response.setCharacterEncoding("utf-8");
          }


          @Override
          protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

              request.setCharacterEncoding("utf-8");
              response.setCharacterEncoding("utf-8");
              String role_name, positions;

                  role_name = request.getParameter("role_name");
                  positions = request.getParameter("positions");

                  Role role = roleService.add(role_name, positions);
                  if (role != null) {
                      request.getSession().setAttribute("Role", role);
                      request.getRequestDispatcher("registration.jsp").forward(request, response);
                  } else {
                      throw new ServletException();
                  }


          }
      }


