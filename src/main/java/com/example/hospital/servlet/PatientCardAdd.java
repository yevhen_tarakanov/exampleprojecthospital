package com.example.hospital.servlet;


import com.example.hospital.model.PatientCard;
import com.example.hospital.service.PatientCardService;
import com.example.hospital.service.PatientStatusService;
import com.example.hospital.service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "patientCardAdd", value = "/patientcardadd")

public class PatientCardAdd extends HttpServlet {


    private UserService userService ;
    private PatientCardService patientCardService;
    private PatientStatusService patientStatusService;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext =  getServletContext();
        userService = (UserService) servletContext.getAttribute("userService");
        patientCardService = (PatientCardService) servletContext.getAttribute("patientCardService");
        patientStatusService = (PatientStatusService) servletContext.getAttribute("patientStatusService");
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        final HttpSession session = request.getSession();
        String roles =(String) session.getAttribute("roles");
        String login =(String) session.getAttribute("login");

        request.setAttribute ("listUser", userService.getAllPatient(roles,login));
        request.setAttribute ("listUserNo", userService.getDoctor());

        request.setAttribute ("listStatus", patientStatusService.allStatus());

        request.getRequestDispatcher("/patientCard/patientCardAdd.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        String diagnosis, treatment;
        String user_id,doctor_id,drug, operations, status_id;

        try {
            user_id = request.getParameter("user_id");
            doctor_id =request.getParameter("doctor_id");
            diagnosis = request.getParameter("diagnosis");
            treatment = request.getParameter("treatment");
            drug = request.getParameter("drug");
            operations = request.getParameter("operations");
            status_id = request.getParameter("status_id");


            PatientCard patientCard = patientCardService.add(user_id, doctor_id, diagnosis, treatment, drug, operations, status_id);
            if (patientCard != null) {
                request.getSession().setAttribute("PatientCard", patientCard);
                response.sendRedirect("/patientcard");
            } else {
                response.sendRedirect("/error");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
