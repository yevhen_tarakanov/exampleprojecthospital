package com.example.hospital.servlet;

import com.example.hospital.service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

@WebServlet(name = "doctor",value = "/doctor")
public class DoctorMain extends HttpServlet {

    private UserService userService ;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext =  getServletContext();
        userService = (UserService) servletContext.getAttribute("userService");
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        final HttpSession session = request.getSession();
        String roles =(String) session.getAttribute("roles");
        String login =(String) session.getAttribute("login");

        ArrayList<Map<String, String>> listDoctor ;
        listDoctor = userService.getAllDoctor(roles,login);

        ArrayList<Map<String, String>> listAmountPatient ;
        listAmountPatient =  userService.getAmountPatient();

        for (int k = 0; k < listAmountPatient.size(); k++) {
            for (int i = 0; i < listDoctor.size(); i++) {
                if (Objects.equals(listDoctor.get(i).get("id"), listAmountPatient.get(k).get("id"))) {
                    listDoctor.get(i).put("sum", String.valueOf(listAmountPatient.get(k).get("sum")));
                }
            }
        }

        request.setAttribute("listdoctor", listDoctor);
        request.getRequestDispatcher("/doctors/doctor-main.jsp").forward(request, response);

    }



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
    }



}
