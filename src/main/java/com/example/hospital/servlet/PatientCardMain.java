package com.example.hospital.servlet;

import com.example.hospital.service.PatientCardService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "patientcard",value = "/patientcard")
public class PatientCardMain extends HttpServlet {


    private PatientCardService patientCardService;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext =  getServletContext();
        patientCardService = (PatientCardService) servletContext.getAttribute("patientCardService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        final HttpSession session = request.getSession();
        String roles =(String) session.getAttribute("roles");
        String role_id =(String) session.getAttribute("roleid");
        String id =(String) session.getAttribute("id");


            request.setAttribute("listCard", patientCardService.getAllCards(roles,role_id,id));

        request.getRequestDispatcher("/patientCard/patientCardMain.jsp").forward(request,response);

    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
    }


}
