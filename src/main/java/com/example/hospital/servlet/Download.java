package com.example.hospital.servlet;

import com.example.hospital.service.PatientCardService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.Objects;

@WebServlet(name = "download",value = "/download")
public class Download extends HttpServlet {

    private PatientCardService patientCardService ;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext =  getServletContext();
        patientCardService = (PatientCardService) servletContext.getAttribute("patientCardService");
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
    //        create doc
        PDDocument documentFile = new PDDocument();
        for (int i = 0; i < 1; i++) {
            PDPage my_page = new PDPage();
            documentFile.addPage(my_page);
        }
        documentFile.save("D:/IdeaProjects/hospital/src/main/resources/filePDF/PatientCard.pdf");
        documentFile.close();

        File file = new File("D:/IdeaProjects/hospital/src/main/resources/filePDF/PatientCard.pdf");
        PDDocument documentFileNew = PDDocument.load(file);

        PDPage page = documentFileNew.getPage(0);
        PDPageContentStream contentStream = new PDPageContentStream(documentFileNew, page);

        String cardnumber = req.getParameter("cardnumber");
//        String language = req.getParameter("language");

        final HttpSession session = req.getSession();
        String roles =(String) session.getAttribute("roles");
        String role_id =(String) session.getAttribute("roleid");
        String id =(String) session.getAttribute("id");

        String language = (String) session.getAttribute("language");
        System.out.println("language " + language);

        for (Map<String, String> stringMap : patientCardService.getAllCards(roles,role_id,id)) {
            String idCardNum = stringMap.get("idCard");

            if (Objects.equals(idCardNum, cardnumber)) {

                contentStream.beginText();
                PDFont font = PDType0Font.load(documentFileNew, new File("c:/windows/fonts/times.ttf"));
                contentStream.setFont(font, 16);
                contentStream.newLineAtOffset(30, 725);
                contentStream.setLeading(14.5f);

                String idCard = stringMap.get("idCard");
                String idd = stringMap.get("id");
                String firstname = stringMap.get("firstname");
                String lastname = stringMap.get("lastname");

                String diagnosis = stringMap.get("diagnosis");
                String treatment = stringMap.get("treatment");
                String drug = stringMap.get("drug");
                String operations = stringMap.get("operations");

                String firstnameDoc = stringMap.get("firstnameDoc");
                String lastnameDoc = stringMap.get("lastnameDoc");
                String patronymicDoc = stringMap.get("patronymicDoc");

                String role_idDoc = stringMap.get("role_idDoc");
                String role_name = stringMap.get("role_name");
                String role_positions = stringMap.get("role_positions");

                String status = stringMap.get("status");

                if (Objects.equals(language, "en")) {
                    contentStream.showText("numbers Id Card: " + "  " + idCard);
                    contentStream.newLine();
                    contentStream.showText("id: " + "  " + idd);
                    contentStream.newLine();
                    contentStream.showText("lastname: " + "  " + lastname);
                    contentStream.newLine();
                    contentStream.showText("firstname: " + "  " + 	firstname);
                    contentStream.newLine();

                    contentStream.showText("diagnosis: " + "  " + diagnosis);
                    contentStream.newLine();
                    contentStream.showText("treatment: " + "  " + treatment);
                    contentStream.newLine();
                    contentStream.showText("drug: " + "  " + drug);
                    contentStream.newLine();
                    contentStream.showText("operations: " + "  " + operations);
                    contentStream.newLine();

                    contentStream.showText("firstname Doc: " + "  " + firstnameDoc);
                    contentStream.newLine();
                    contentStream.showText("lastname Doc: " + "  " + lastnameDoc);
                    contentStream.newLine();
                    contentStream.showText("patronymic Doc: " + "  " + patronymicDoc);
                    contentStream.newLine();
                    contentStream.showText("role id Doc: " + "  " + role_idDoc);
                    contentStream.newLine();
                    contentStream.showText("role name: " + "  " + role_name);
                    contentStream.newLine();
                    contentStream.showText("role positions: " + "  " + role_positions);
                    contentStream.newLine();

                    contentStream.showText("status: " + "  " + status);
                    contentStream.endText();
                }
                if (Objects.equals(language, "uk")) {
                    contentStream.showText("Номер картки: " + "  " + idCard);
                    contentStream.newLine();
                    contentStream.showText("id паціента: " + "  " + idd);
                    contentStream.newLine();
                    contentStream.showText("Прізвище: " + "  " + firstname);
                    contentStream.newLine();
                    contentStream.showText("Ім'я: " + "  " + lastname);
                    contentStream.newLine();

                    contentStream.showText("Діагноз: " + "  " + diagnosis);
                    contentStream.newLine();
                    contentStream.showText("Лікування: " + "  " + treatment);
                    contentStream.newLine();
                    contentStream.showText("Ліки: " + "  " + drug);
                    contentStream.newLine();
                    contentStream.showText("Операції: " + "  " + operations);
                    contentStream.newLine();

                    contentStream.showText("Ім'я Доктора: " + "  " + firstnameDoc);
                    contentStream.newLine();
                    contentStream.showText("Прізвище Доктора: " + "  " + lastnameDoc);
                    contentStream.newLine();
                    contentStream.showText("По батькові Доктора: " + "  " + patronymicDoc);
                    contentStream.newLine();
                    contentStream.showText("Ідентифікатор ролі Доктора: " + "  " + role_idDoc);
                    contentStream.newLine();
                    contentStream.showText("Ім'я ролі: " + "  " + role_name);
                    contentStream.newLine();
                    contentStream.showText("Посада: " + "  " + role_positions);
                    contentStream.newLine();

                    contentStream.showText("Статус: " + "  " + status);
                    contentStream.endText();
                }
            }
        }
        contentStream.close();
        documentFileNew.save("D:/IdeaProjects/hospital/src/main/resources/filePDF/PatientCard.pdf");
        documentFileNew.close();

        //-----------upload file--------------

        String path = "D:/IdeaProjects/hospital/src/main/resources/filePDF/PatientCard.pdf";
        resp.setContentType("application/pdf");
        resp.setHeader("Content-disposition", "attachment; filename=PatientCard.pdf");
        try (FileInputStream in = new FileInputStream(path); OutputStream out = resp.getOutputStream()) {
            byte[] buffer = new byte[2096];
            int numBytesRead;
            while ((numBytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, numBytesRead);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
    }

}
