package com.example.hospital.servlet;

import com.example.hospital.model.PatientCard;
import com.example.hospital.service.PatientCardService;
import com.example.hospital.service.PatientStatusService;
import com.example.hospital.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserCardHandler extends HttpServlet {
    private static final String EDIT = "/patientCard/patientCardEdit.jsp";
    private static final String USERCARD = "/patientCard/patientCardMain.jsp";

    private UserService userService ;
    private PatientCardService patientCardService;
    private PatientStatusService patientStatusService;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext =  getServletContext();
        userService = (UserService) servletContext.getAttribute("userService");
        patientCardService = (PatientCardService) servletContext.getAttribute("patientCardService");
        patientStatusService = (PatientStatusService) servletContext.getAttribute("patientStatusService");
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        request.setAttribute ("listDoctor", userService.getDoctor());

        request.setAttribute ("listStatus", patientStatusService.allStatus());

        String redirect = "";
        String action = request.getParameter("action");
         if (action.equalsIgnoreCase("delete")) {
            String id = request.getParameter("id");
            int uid = Integer.parseInt(id);

             patientCardService.delete(uid);
             redirect = USERCARD;
        }
        else if (action.equalsIgnoreCase("editform")) {

            String userId = request.getParameter("idCard");
            PatientCard patientCard ;
            int id = Integer.parseInt(userId);

                patientCard = patientCardService.getById(id);


            patientCard.setIdCard(id);

            request.setAttribute("PatientCard", patientCard);
            redirect = EDIT;
        }
        else if (action.equalsIgnoreCase("edit")) {
            String idCard = request.getParameter("idCard");
            int idc = Integer.parseInt(idCard);
            PatientCard patientCard = new PatientCard();

            patientCard.setIdCard(idc);
            patientCard.setDiagnosis(request.getParameter("diagnosis"));
            patientCard.setTreatment(request.getParameter("treatment"));
            patientCard.setDrug(request.getParameter("drug"));
            patientCard.setOperations(request.getParameter("operations"));
            patientCard.setDoctor_id(Integer.parseInt(request.getParameter("doctor_id")));
            patientCard.setStatus_id(Integer.parseInt(request.getParameter("status_id")));

                patientCardService.update(patientCard);

        }


        RequestDispatcher rd = request.getRequestDispatcher(redirect);
        rd.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        doGet(request, response);
    }
}
