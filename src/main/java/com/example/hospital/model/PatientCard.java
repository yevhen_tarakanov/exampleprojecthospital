package com.example.hospital.model;

import java.util.Objects;

public class PatientCard {
   private int idCard, user_id, doctor_id, status_id;
   private String diagnosis, treatment, drug, operations;

    public PatientCard( String user_id, String doctor_id,String diagnosis, String treatment, String drug, String operations,String status_id) {
        this.user_id = Integer.parseInt(user_id);
        this.doctor_id = Integer.parseInt(doctor_id);
        this.diagnosis = diagnosis;
        this.drug = drug;
        this.operations = operations;
        this.treatment = treatment;
        this.status_id = Integer.parseInt(status_id);
    }

    public PatientCard() {
    }

    public int getIdCard() {
        return idCard;
    }

    public void setIdCard(int idCard) {
        this.idCard = idCard;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(int doctor_id) {
        this.doctor_id = doctor_id;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }

    public String getOperations() {
        return operations;
    }

    public void setOperations(String operations) {
        this.operations = operations;
    }





    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientCard that = (PatientCard) o;
        return idCard == that.idCard && user_id == that.user_id && doctor_id == that.doctor_id && status_id == that.status_id && Objects.equals(diagnosis, that.diagnosis) && Objects.equals(treatment, that.treatment) && Objects.equals(drug, that.drug) && Objects.equals(operations, that.operations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCard, user_id, doctor_id, status_id, diagnosis, treatment, drug, operations);
    }

    @Override
    public String toString() {
        return "PatientCard{" +
                "idCard=" + idCard +
                ", user_id=" + user_id +
                ", doctor_id=" + doctor_id +
                ", status_id=" + status_id +
                ", diagnosis='" + diagnosis + '\'' +
                ", treatment='" + treatment + '\'' +
                ", drug='" + drug + '\'' +
                ", operations='" + operations + '\'' +
                '}';
    }
}
