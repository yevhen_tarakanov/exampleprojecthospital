package com.example.hospital.model;

import java.sql.Date;
import java.util.Objects;

public class User {
    private int id;
      private String role_id ;
         private Date birthday;

    private String firstname;
    private String lastname;
    private String gender;
    private String login;
    private String password;
    private String patronymic;
    private String roles;



    public User() {
    }
    public User(int id) {
        this.id = id;
    }
    public User(Date birthday, String firstname, String lastname, String login, String password, String gender, String patronymic, String role_id, String roles) {
        this.birthday = birthday;
        this.firstname = firstname;
        this.lastname = lastname;
        this.login = login;
        this.password = password;
        this.gender = gender;
        this.patronymic = patronymic;
        this.role_id = role_id;
        this.roles = roles;
    }






    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getpatronymic() {
        return patronymic;
    }

    public void setpatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getRole() {
        return this.roles;
    }

    public void setRole(String role) {
        this.roles = role;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(role_id, user.role_id) && Objects.equals(birthday, user.birthday) && Objects.equals(firstname, user.firstname) && Objects.equals(lastname, user.lastname) && Objects.equals(gender, user.gender) && Objects.equals(login, user.login) && Objects.equals(password, user.password)&& Objects.equals(patronymic, user.patronymic )&& Objects.equals(roles, user.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, role_id, birthday, firstname, lastname, gender, login, password,patronymic,roles);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", role_id=" + role_id +
                ", birthday=" + birthday +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", gender='" + gender + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", role='" + roles + '\'' +
                '}';
    }






}
