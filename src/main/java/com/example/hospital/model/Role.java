package com.example.hospital.model;

import java.util.Objects;

public class Role {
    private int role_id;
    private String role_name,positions;

    public Role() {
    }

    public Role( String role_name, String positions) {

        this.role_name = role_name;
        this.positions = positions;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getPosition() {
        return positions;
    }

    public void setPosition(String position) {
        this.positions = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return role_id == role.role_id && Objects.equals(role_name, role.role_name) && Objects.equals(positions, role.positions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(role_id, role_name, positions);
    }

    @Override
    public String toString() {
        return "Role{" +
                "role_id=" + role_id +
                ", role_name='" + role_name + '\'' +
                ", position='" + positions + '\'' +
                '}';
    }
}
