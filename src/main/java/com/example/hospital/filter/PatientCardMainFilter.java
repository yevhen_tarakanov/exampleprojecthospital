package com.example.hospital.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/patientcard")
public class PatientCardMainFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);

        boolean loggedIn = session != null && session.getAttribute("login") != null && session.getAttribute("roles") != null;
        if (loggedIn) {
            String userRole = session.getAttribute("roles").toString();
            if (userRole.equals("ADMIN")||userRole.equals("USER")||userRole.equals("DOC")||userRole.equals("NURSE")) {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/");
        }
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
