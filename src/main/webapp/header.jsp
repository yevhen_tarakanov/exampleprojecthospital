﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>


<header>
    <div class="container ">
        <div class="d-flex flex-column flex-md-row align-items-center mb-4 border-bottom ">

            <a href="<c:url value="/"/>" class="d-flex align-items-center ukraine">
                <span class="fs-1 "><fmt:message key="Hospital.2"/></span>
            </a>

            <nav class="d-inline-flex align-items-center mt-2 mt-md-0 ms-md-auto ">
                <c:choose>
                    <c:when test="${sessionScope.get('roles')=='ADMIN'}">
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patient"/>"><fmt:message key="Patient"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patientcard"/>"><fmt:message key="Patient.card"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/registration"/>"><fmt:message key="Add.a.new.patient"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patientcardadd"/>"><fmt:message key="Create.a.patient.card"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/doctor"/>"><fmt:message
                                key="Doctors"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/doctor/add"/>"><fmt:message key="Doctor.post.additions"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/registration"/>"><fmt:message key="Registration"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/about"/>"><fmt:message
                                key="About.project"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/logout"/>"><fmt:message
                                key="Logout"/></a>
                    </c:when>
                    <c:when test="${sessionScope.get('roles')=='DOC'}">
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patient"/>"><fmt:message key="Patient"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patientcard"/>"><fmt:message key="Patient.card"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/registration"/>"><fmt:message key="Add.a.new.patient"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patientcardadd"/>"><fmt:message key="Create.a.patient.card"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/doctor"/>"><fmt:message
                                key="Doctors"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/logout"/>"><fmt:message
                                key="Logout"/></a>
                    </c:when>
                    <c:when test="${sessionScope.get('roles')=='USER'}">
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patient"/>"><fmt:message key="Patient"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patientcard"/>"><fmt:message key="Patient.card"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/registration"/>"><fmt:message key="Add.a.new.patient"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/doctor"/>"><fmt:message
                                key="Doctors"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/logout"/>"><fmt:message
                                key="Logout"/></a>
                    </c:when>
                    <c:when test="${sessionScope.get('roles')=='NURSE'}">
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patient"/>"><fmt:message key="Patient"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patientcard"/>"><fmt:message key="Patient.card"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/registration"/>"><fmt:message key="Add.a.new.patient"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/patientcardadd"/>"><fmt:message key="Create.a.patient.card"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/doctor"/>"><fmt:message
                                key="Doctors"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/logout"/>"><fmt:message
                                key="Logout"/></a>
                    </c:when>
                    <c:otherwise>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/"/>"><fmt:message
                                key="Main"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/authorization"/>"><fmt:message key="Authorization"/> </a>
                        <a class="me-3 py-2 text-dark text-decoration-none"
                           href="<c:url value="/registration"/>"><fmt:message key="Registration"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/about"/>"><fmt:message
                                key="About.project"/></a>
                        <a class="me-3 py-2 text-dark text-decoration-none" href="<c:url value="/logout"/>"><fmt:message
                                key="Logout"/></a>
                    </c:otherwise>
                </c:choose>

            </nav>




            <div class="lan">
                <form class="lan">
                    <select size="2" class="select-lan" id="language" name="language" onchange="submit()">
                        <option class="icon-ok" value="uk" ${language == 'uk' ? 'selected' : ''}><fmt:message key="ukraine"/></option>
                        <option class="icon-en" value="en" ${language == 'en' ? 'selected' : ''}><fmt:message key="english"/></option>
                    </select>
                </form>
            </div>

            <div class="wrap">
                <label class="button">
                    <input type="checkbox" id="themer">
                    Dark : <span aria-hidden="true"></span>
                </label>

            </div>
            <script src="js/theme.js"></script>

            <c:choose>
                <c:when test="${sessionScope.get('roles')!=null}">
                    <div class="loginfo">
                        <h4 class="loginfo-context"><fmt:message key="user.information"/> !</h4>
                        <h5 class="loginfo-context"><strong><fmt:message key="Login"/>
                            :</strong> ${sessionScope.get("login")}</h5>
                        <h5 class="loginfo-context"><strong><fmt:message key="Role"/>
                            :</strong> ${sessionScope.get("roles")}</h5>
                    </div>
                </c:when>

                <c:otherwise>
                    <div class="loginfo">
                        <h4 class="loginfo-context"><fmt:message key="user.information"/> !</h4>
                    </div>
                </c:otherwise>
            </c:choose>


        </div>
    </div>


</header>