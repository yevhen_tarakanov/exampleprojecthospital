﻿const form = document.querySelector('.formRegValidation');

const validateBtn = form.querySelector('.validateBtn');

const password = form.querySelector('.password');
const passwordConfirmation = form.querySelector('.passwordConfirmation');

const genderMen = form.querySelector(".genderMen");
const genderWoman = form.querySelector(".genderWoman");

const fields = form.querySelectorAll('.field')




const generateError = function (text) {
    const error = document.createElement("div");
    error.className = "error";
    error.style.color = "#ff0000";
    error.style.width = "auto";
    error.style.fontFamily = "cursive";
    error.style.fontSize = "15px";
    error.style.background = "#e8e8e8"
    error.innerHTML = text;
    return error;
};

const removeValidation = function () {
    const errors = form.querySelectorAll('.error');
    for (let i = 0; i < errors.length; i++) {
        errors[i].remove();
    }
};

const checkFieldsPresence = function () {
    for (let i = 0; i < fields.length; i++) {
        if (!fields[i].value) {
            const error = generateError("Cant be blank");
            form[i].parentElement.insertBefore(error, fields[i]);
        }
    }
};
//password
let checkPasswordMatch = function () {
    if (password.value !== passwordConfirmation.value) {
        const error = generateError("Password doesnt match");
        password.parentElement.insertBefore(error, password);
        return false;
    }
    return true;
};
//checkCheckbox
const checkCheckbox = function (){
    if (!genderMen.checked&&!genderWoman.checked) {
        const errors = generateError("Checkbox not checked");
        genderMen.parentElement.insertBefore(errors, genderMen);
        return false;
    }
    return true;
};

form.addEventListener("submit",function (event) {

      if  (checkPasswordMatch()=== false){
          event.preventDefault();
      }
        for (let i = 0; i < fields.length; i++) {
            if (!fields[i].value) {
                event.preventDefault();
                removeValidation();
                checkFieldsPresence();
                checkPasswordMatch();
                checkCheckbox();
            }
        }
});

