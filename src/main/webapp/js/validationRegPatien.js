﻿const form = document.querySelector('.validationRegPatien');
const fields = form.querySelectorAll('.field')
const generateError = function (text) {
    const error = document.createElement("div");
    error.className = "error";
    error.style.color = "#ff0000";
    error.style.width = "auto";
    error.style.fontFamily = "cursive";
    error.style.fontSize = "15px";
    error.style.background = "#e8e8e8"
    error.innerHTML = text;
    return error;
};
const removeValidation = function () {
    const errors = form.querySelectorAll('.error');
    for (let i = 0; i < errors.length; i++) {
        errors[i].remove();
    }
};
const checkFieldsPresence = function () {
    for (let i = 0; i < fields.length; i++) {
        if (!fields[i].value) {
            const error = generateError("Cant be blank");
            form[i].parentElement.insertBefore(error, fields[i]);
        }
    }
};
form.addEventListener("submit",function (event) {
    for (let i = 0; i < fields.length; i++) {
        if (!fields[i].value) {
            event.preventDefault();
            removeValidation();
            checkFieldsPresence();
        }
        if (fields[i].value) {
        }
    }
});

