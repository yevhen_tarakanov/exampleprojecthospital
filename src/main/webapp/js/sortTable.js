﻿function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("userTable");
    switching = true;
    //Установите направление сортировки по возрастанию:
    dir = "asc";
    /*Сделайте цикл, который будет продолжаться до тех пор, пока
     переключения не было:*/
    while (switching) {
        //начнем с того, что переключение не выполняется:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Перебрать все строки таблицы (кроме
         первый, который содержит заголовки таблиц):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //начнем с того, что не должно быть переключения:
            shouldSwitch = false;
            /*Получите два элемента, которые вы хотите сравнить,
             один из текущей строки и один из следующего:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*проверьте, должны ли две строки поменяться местами,
             в зависимости от направления, по возрастанию или по убыванию:*/
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //если это так, пометьте как переключатель и разорвите цикл:
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //если это так, пометьте как переключатель и разорвите цикл:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /*Если переключатель отмечен, сделайте его
             и отметьте, что переключение было сделано:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Каждый раз, когда выполняется переключение, увеличивайте это значение на 1:
            switchcount++;
        } else {
            /*Если переключение не было выполнено И направление "возрастание",
             установите направление на «desc» и снова запустите цикл while.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}