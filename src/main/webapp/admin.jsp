﻿
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />




<html lang="${language}">
<head>
    <title><fmt:message key="Hospital.1" /></title>
    <link rel="stylesheet" href="CSS/main.css">
    <link rel="stylesheet" href="CSS/mainMe.css">

    <link rel="stylesheet" href="CSS/theme.css"  />
    <style id="inverter" media="none">
        html {
            background-color: #eee;
            filter: invert(100%)
        }
        .ukraine:not([src*=".svg"]),
        [style*="url("] {
            filter: invert(100%);
        }
    </style>
</head>
<body>
        <header>
            <jsp:include page="header.jsp" />
        </header>


    <main>
       <h1 class="admin-page"><fmt:message key="admin.page" /> </h1>
</main>

        <div class="contentt"></div>
        <footer>
            <jsp:include page="footer.jsp" />
        </footer>
</body>
</html>
