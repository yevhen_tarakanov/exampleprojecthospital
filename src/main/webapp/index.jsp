<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <title><fmt:message key="Hospital.1" /></title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <%--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">--%>
        <link rel="stylesheet" href="CSS/main.css">
        <link rel="stylesheet" href="CSS/mainMe.css">


        <link href="CSS/theme.css" rel="stylesheet" />
        <style id="inverter" media="none">
            html {
                background-color: #eee;
                filter: invert(100%);
            }
            .ukraine:not([src*=".svg"]),
            [style*="url("] {
                filter: invert(100%);
            }
        </style>
    </head>

    <body>

        <header>
           <jsp:include page="header.jsp" />
        </header>

        <main>
            <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light ">
                <div class="col-md-5 p-lg-5 mx-auto my-auto">
                    <h1 class="display-4 fw-bold w-auto"><fmt:message key="Make.your.choice" /></h1>
                    <br>
                    <a class="btn btn-outline-secondary fw-bold button-reg1" href="<c:url value="/authorization"/>"><fmt:message key="Sing.in" /> </a>
                    <a class="btn btn-outline-secondary fw-bold button-reg2" href="<c:url value="/registration"/>"><fmt:message key="Registration" /></a>
                </div>
            </div>
        </main>





        <footer>
            <jsp:include page="footer.jsp" />
        </footer>

    </body>
</html>
