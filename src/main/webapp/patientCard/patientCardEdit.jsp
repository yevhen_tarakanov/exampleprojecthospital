<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="PatientCard" scope="request" type="com.example.hospital.model.PatientCard"/>
<jsp:useBean id="listDoctor" scope="request" type="java.util.List"/>
<jsp:useBean id="listStatus" scope="request" type="java.util.List"/>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />



<!DOCTYPE HTML>
<html lang="${language}">
<head>
    <title><fmt:message key="Patient" /></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<%--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">--%>
    <link rel="stylesheet" href="../CSS/main.css">
    <link rel="stylesheet" href="../CSS/mainMe.css">

    <link rel="stylesheet" href="/CSS/theme.css"  />
    <style id="inverter" media="none">
        html {
            background-color: #eee;
            filter: invert(100%)
        }
        .ukraine:not([src*=".svg"]),
        [style*="url("] {
            filter: invert(100%);
        }
    </style>
</head>
<body>
<header>
    <jsp:include page="/header.jsp" />
</header>


        <div class="container mt-5 mb-5">
    <h1><fmt:message key="Patient.card" /></h1>

    <form method="POST" action='UserCardHandler' name="frmEditUser">
        <input
                type="hidden" name="action" value="edit"/>
        <table>
            <tr>
                <td><fmt:message key="User.ID" /></td>
                <td>
                    <label>
                        <input style="background-color: #d4d4d4" type="text" name="idCard" readonly="readonly"
                               value= ${PatientCard.idCard}>

                    </label>
                </td>
            </tr>

            <tr>
                <td><fmt:message key="Diagnosis" /></td>
                <td><label>
                    <input type="text" name="diagnosis" value= ${PatientCard.diagnosis}>
                </label></td>
            </tr>

            <tr>
                <td><fmt:message key="Treatment" /></td>
                <td><label>
                    <input type="text" name="treatment" value= ${PatientCard.treatment}>
                </label></td>
            </tr>

            <tr>
                <td><fmt:message key="Drug" /></td>
                <td><label>
                    <input type="text" name="drug" value= ${PatientCard.drug}>
                </label></td>
            </tr>
            <tr>
                <td><fmt:message key="Operations" /></td>
                <td><label>
                    <input type="text" name="operations" value= ${PatientCard.operations}>
                </label></td>
            </tr>

            <tr>
                <td><fmt:message key="Doctors" /> </td>

                <td><label>
                    <select  class="select-sel" name="doctor_id">
                        <option value="${PatientCard.doctor_id}" selected>${PatientCard.doctor_id}</option>

                        <c:forEach items="${listDoctor}" var="ld" >
                        <option value="${ld.id}">${ld.id}--${ld.role_name}--${ld.lastname }--${ld.firstname}--${ld.positions}</option>
                        </c:forEach>
                    </select>
                </label></td>
            </tr>

            <tr>
                <td><fmt:message key="Status" /></td>
                <td>
                    <label>
                    <select class="select-sel" name="status_id">

                            <option value="${PatientCard.status_id}" selected > ${PatientCard.status_id}</option>
                        <c:forEach items="${listStatus}" var="ls" >
                            <option value="${ls.id}" > ${ls.id}--${ls.status}</option>
                        </c:forEach>
                    </select>
                </label>
                </td>
            </tr>
            <tr>
                <td> </td>
                <td><input class="button-update bu" type="submit" value="<fmt:message key="Update" />"/></td>
            </tr>
        </table>
    </form>
</div>


        <footer>
            <jsp:include page="/footer.jsp" />
        </footer>

</body>
</html>