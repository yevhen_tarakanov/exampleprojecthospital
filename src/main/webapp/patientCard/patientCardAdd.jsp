<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="listUser" scope="request" type="java.util.List"/>
<jsp:useBean id="listUserNo" scope="request" type="java.util.List"/>
<jsp:useBean id="listStatus" scope="request" type="java.util.List"/>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html lang="${language}">
<head>
    <title><fmt:message key="Patient" /></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="../CSS/main.css">
    <link rel="stylesheet" href="../CSS/mainMe.css">

    <link rel="stylesheet" href="/CSS/theme.css"  />
    <style id="inverter" media="none">
        html {
            background-color: #eee;
            filter: invert(100%)
        }
        .ukraine:not([src*=".svg"]),
        [style*="url("] {
            filter: invert(100%);
        }
    </style>
</head>
<body>
<header>
    <jsp:include page="/header.jsp" />
</header>

<div class="container mt-5 mb-5">
    <div class="text-center" style="margin-left: 60px">
    <h1><fmt:message key="Patient.card" /></h1>
    </div>

    <form action="/patientcardadd" method="post" class="container mt-sm-auto mb-sm-5 border-form validationRegPatien">

            <div >
        <tr >
            <td><fmt:message key="Patient" /> : </td>
            <td><label>
                <select class="select-sel field" name="user_id" class="mt-3">
                    <c:forEach items="${listUser}" var="listUser" >
                        <option selected></option>
                        <option value="${listUser.id}" > ${listUser.id}--${listUser.lastname}--${listUser.firstname}--${listUser.birthday}</option>
                    </c:forEach>
                </select>
            </label></td>
        </tr>
            </div>
        <br> <br>
        <tr>
            <td><fmt:message key="Doctors" /> : </td>
            <td><label>
                <select class="select-sel field" name="doctor_id" class="m-auto mb-2">
                    <c:forEach items="${listUserNo}" var="listUserNo" >
                        <option selected></option>
                        <option value="${listUserNo.id}" > ${listUserNo.id}--${listUserNo.lastname}--${listUserNo.firstname}--${listUserNo.birthday}</option>
                    </c:forEach>
                </select>
            </label></td>
        </tr>
        <br> <br>

        <div class="form-floating">
            <input type="text" name="diagnosis"  class="form-control field" id="floatingDiagnosis" placeholder="Диагноз">
            <label for="floatingDiagnosis"><fmt:message key="Diagnosis" /></label>
        </div>
        <br>
        <div class="form-floating">
            <input type="text" name="treatment"  class="form-control field" id="floatingTreatment" placeholder="Лечение">
            <label for="floatingTreatment"><fmt:message key="Treatment" /></label>
        </div>
        <br>
        <div class="form-floating">
            <input type="text" name="drug"  class="form-control field" id="floatingDrug" placeholder="Ліки">
            <label for="floatingDrug"><fmt:message key="Drug" /></label>
        </div>
        <br>
        <div class="form-floating">
            <input type="text" name="operations"  class="form-control field" id="floatingOperations" placeholder="Операція">
            <label for="floatingTreatment"><fmt:message key="Operations" /></label>
        </div>
        <br>
        <tr>
            <td><fmt:message key="Status.of.the.patient" /></td>
            <td><label>
                <select class="select-sel field" name="status_id">
                    <c:forEach items="${listStatus}" var="listStatus" >
                        <option selected></option>
                        <option value="${listStatus.id}" > ${listStatus.id}--${listStatus.status}</option>
                    </c:forEach>
                </select>
            </label></td>
        </tr>
    <br> <br>


        <div class="text-center">
        <button type="submit" class="btn button-reg-btn"><fmt:message key="Fill.out.a.patient.card" /></button>
        </div>
    </form>
</div>
<script src="<c:url value="/js/validationRegPatien.js"/>"></script>

<footer>
    <jsp:include page="/footer.jsp" />
</footer>
</body>
</html>