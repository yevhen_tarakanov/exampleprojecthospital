﻿
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:useBean id="listCard" scope="request" type="java.util.List"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--<jsp:useBean id="listCard2" scope="request" type="java.util.List"/>--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />


<script src="js/sortTable.js"></script>
<script src="js/tableSearch.js"></script>


<!DOCTYPE html>
<html lang="${language}">

<head>
    <title><fmt:message key="List.of.patients" /></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="CSS/main.css">
    <link rel="stylesheet" href="CSS/mainMe.css">

    <link rel="stylesheet" href="CSS/theme.css"  />
    <style id="inverter" media="none">
        html {
            background-color: #eee;
            filter: invert(100%)
        }
        .ukraine:not([src*=".svg"]),
        [style*="url("] {
            filter: invert(100%);
        }
    </style>
</head>
<body>
<header>
    <jsp:include page="/header.jsp" />
</header>


<main>
    <div class="container mt-5">
        <h1><fmt:message key="Patient.card" /></h1>
    </div>


    <div class="search">
        <span><fmt:message key="Search" />:</span>
        <input class="search-input" type="text" placeholder="<fmt:message key="SearchAll" />" id="search-text" onkeyup="tableSearch()">
    </div>


    <div>
        <table id="userTable">
            <tr>
                <th onclick="sortTable(0)"><fmt:message key="id.Card" />&#8595;&#8593;</th>
                <th onclick="sortTable(1)"><fmt:message key="id" />&#8595;&#8593;</th>
                <th onclick="sortTable(2)"><fmt:message key="Lastname" />&#8595;&#8593;</th>
                <th onclick="sortTable(3)"><fmt:message key="Diagnosis" />&#8595;&#8593;</th>
                <th onclick="sortTable(4)"><fmt:message key="Treatment" />&#8595;&#8593;</th>
                <th onclick="sortTable(5)"><fmt:message key="Drug" />&#8595;&#8593;</th>
                <th onclick="sortTable(6)"><fmt:message key="Operations" />&#8595;&#8593;</th>
                <th onclick="sortTable(7)"><fmt:message key="Lastname.Doc" />&#8595;&#8593;</th>
                <th onclick="sortTable(8)"><fmt:message key="Firstname.Doc" />&#8595;&#8593;</th>
                <th onclick="sortTable(9)"><fmt:message key="Patronymic.Doc" />&#8595;&#8593;</th>
                <th onclick="sortTable(10)"><fmt:message key="Role.id.Doc" />&#8595;&#8593;</th>
                <th onclick="sortTable(11)"><fmt:message key="Position" />&#8595;&#8593;</th>
                <th onclick="sortTable(12)"><fmt:message key="Role.Doc" />&#8595;&#8593;</th>
                <th onclick="sortTable(13)"><fmt:message key="Status" />&#8595;&#8593;</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${listCard}" var="lc" >
                <tr>
                    <td class="numbers">${lc.idCard}</td>
                    <td>${lc.id}</td>
                    <td>${lc.lastname}</td>
                    <td>${lc.diagnosis}</td>
                    <td>${lc.treatment}</td>
                    <td>${lc.drug}</td>
                    <td>${lc.operations}</td>
                    <td>${lc.lastnameDoc}</td>
                    <td>${lc.firstnameDoc}</td>
                    <td>${lc.patronymicDoc}</td>
                    <td>${lc.role_idDoc}</td>
                    <td>${lc.role_name}</td>
                    <td>${lc.role_positions}</td>
                    <td>${lc.status}</td>

                        <%--                    <td><a class="button-table" href="download?cardnumber=${lc.idCard}&language=en">English</a>--%>
                        <%--                        <br>--%>
                        <%--                        <a class="button-table" href="download?cardnumber=${lc.idCard}&language=uk">Ukraine</a>--%>
                        <%--                    </td>--%>
                    <td><a class="button-table" href="download?cardnumber=${lc.idCard}"><fmt:message key="Download" /></a></td>

                    <c:choose>
                        <c:when test="${sessionScope.get('roles')=='ADMIN'}">
                            <td><a class="button-table" href="UserCardHandler?action=editform&idCard=${lc.idCard}"><fmt:message key="Update" /></a></td>
                            <td><a class="button-table" href="UserCardHandler?action=delete&id=${lc.idCard}"><fmt:message key="Remove" /></a></td>
                        </c:when>
                        <c:when test="${sessionScope.get('roles')=='USER'}">
                            <td></td>
                            <td></td>
                        </c:when>
                        <c:when test="${sessionScope.get('roles')=='NURSE'}">
                            <td><a class="button-table" href="UserCardHandler?action=editform&idCard=${lc.idCard}"><fmt:message key="Update" /></a></td>
                            <td><a class="button-table" href="UserCardHandler?action=delete&id=${lc.idCard}"><fmt:message key="Remove" /></a></td>
                        </c:when>
                        <c:when test="${sessionScope.get('roles')=='DOC'}">
                            <td><a class="button-table" href="UserCardHandler?action=editform&idCard=${lc.idCard}"><fmt:message key="Update" /></a></td>
                            <td><a class="button-table" href="UserCardHandler?action=delete&id=${lc.idCard}"><fmt:message key="Remove" /></a></td>
                        </c:when>
                        <c:otherwise>

                        </c:otherwise>
                    </c:choose>
<%--                    <td><a class="button-table" href="UserCardHandler?action=editform&idCard=${lc.idCard}"><fmt:message key="Update" /></a></td>--%>
<%--                    <td><a class="button-table" href="UserCardHandler?action=delete&id=${lc.idCard}"><fmt:message key="Remove" /></a></td>--%>



            </c:forEach>






        </table>
    </div>
</main>

<footer>
    <jsp:include page="/footer.jsp" />
</footer>


</body>
</html>
