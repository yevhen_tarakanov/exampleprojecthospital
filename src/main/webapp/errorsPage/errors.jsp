﻿<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />


<!DOCTYPE html>
<html lang="${language}">
<head>
    <title><fmt:message key="Hospital.1" /></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="../CSS/main.css">
    <link rel="stylesheet" href="../CSS/errors.css">
    <link rel="stylesheet" href="../CSS/mainMe.css">

    <link rel="stylesheet" href="../CSS/theme.css"  />
    <style id="inverter" media="none">
        html {
            background-color: #eee;
            filter: invert(100%)
        }
        .ukraine:not([src*=".svg"]),
        [style*="url("] {
            filter: invert(100%);
        }
    </style>
</head>
<body>
<header>
    <jsp:include page="../header.jsp" />
</header>
<main>
    <br>
    <div class="text-center" >
        <h1 ><span>E&nbsp;&nbsp;</span><span>r&nbsp;&nbsp;</span><span>r&nbsp;&nbsp;</span><span>o&nbsp;&nbsp;</span><span>r&nbsp;</span></h1>
        <h2 ><span><fmt:message key="arose" /></span> <span><fmt:message key="unpredictable" /></span> <span><fmt:message key="error" /></span> !!!</h2>
    </div>
    <br><br><br><br>
</main>
<footer>
    <jsp:include page="../footer.jsp" />
</footer>
</body>

</html>