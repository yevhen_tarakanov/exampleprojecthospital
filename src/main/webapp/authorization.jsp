<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />


<!DOCTYPE html>
<html lang="${language}">


        <head>
            <title><fmt:message key="Authorization" /></title>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%--            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">--%>
            <link rel="stylesheet" href="CSS/main.css">
            <link rel="stylesheet" href="CSS/mainMe.css">
            <link rel="stylesheet" href="CSS/theme.css"  />
            <style id="inverter" media="none">
                html {
                    background-color: #eee;
                    filter: invert(100%)
                }
                .ukraine:not([src*=".svg"]),
                [style*="url("] {
                    filter: invert(100%);
                }
            </style>
        </head>

    <body>
        <header>
            <jsp:include page="header.jsp" />
        </header>

        <main>
            <div class="container mt-sm-5 mb-sm-5 bg-white">
               <div class="text-center">
                    <form action="<c:url value="/authorization"/>" method="post">
                        <h1 class="h3 mb-3 fw-normal fw-bold"><fmt:message key="Login.to.the.system" /></h1>
                        <div class="form-floating">

                            <c:choose>
                                <c:when test="${sessionScope.get('login')==null}">
                                    <input type="login" name="logins"  class="form-control " id="floatingLogin" placeholder="Login">
                                    <label  for="floatingLogin"><fmt:message key="Login" /></label>
                                </c:when>
                                <c:otherwise>
                                    <input type="login" name="logins"  class="form-control " id="floatingLogin" placeholder="Login">
                                    <label for="floatingLogin">${sessionScope.get("login")}</label>

                                </c:otherwise>
                            </c:choose>

<%--                            <input type="login" name="logins"  class="form-control" id="floatingLogin" placeholder="Login">--%>
<%--                            <label for="floatingLogin"><fmt:message key="Login" /></label>--%>
                        </div>
                        <br>
                        <div class="form-floating">
                            <c:choose>
                                <c:when test="${sessionScope.get('password')==null}">
                                    <input type="password" name="passwords"  class="form-control" id="floatingPassword" placeholder="Password">
                                    <label for="floatingPassword"><fmt:message key="Password" /></label>
                                </c:when>
                                <c:otherwise>
                                    <input type="password" name="passwords"  class="form-control" id="floatingLogin" placeholder="Password">
                                    <label for="floatingLogin">${sessionScope.get("password")}</label>
                                </c:otherwise>
                            </c:choose>
<%--                            <input type="password" name="passwords"  class="form-control" id="floatingPassword" placeholder="Password">--%>
<%--                            <label for="floatingPassword"><fmt:message key="Password" /></label>--%>
                        </div>
                        <br>
                        <button type="submit" class="w-60 btn btn-lg btn-primary"><fmt:message key="Sing.in" /></button>
                        <br><br>
                        <a class="w-60 btn btn-lg btn-primary" href="<c:url value="/registration"/>"><fmt:message key="Registration" /></a>
                        <br>
                    </form>
                </div>
            </div>
        </main>

        <footer>
            <jsp:include page="footer.jsp" />
        </footer>

    </body>
</html>
