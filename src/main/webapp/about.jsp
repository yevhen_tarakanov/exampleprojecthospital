﻿<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />


<!DOCTYPE html>
<html lang="${language}">
<head>
  <title><fmt:message key="About.project" /></title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <%--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">--%>
    <link rel="stylesheet" href="CSS/mainMe.css">
    <link rel="stylesheet" href="CSS/main.css">
  <link rel="stylesheet" href="CSS/errors.css">


  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Patrick+Hand&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="CSS/theme.css"  />
    <style id="inverter" media="none">
        html {
            background-color: #eee;
            filter: invert(100%)
        }
        .ukraine:not([src*=".svg"]),
        [style*="url("] {
            filter: invert(100%);
        }
    </style>
</head>
<body>
    <header>
        <jsp:include page="header.jsp" />
    </header>

<main>
        <div class="about">
                    <h1 class="about-h1"><fmt:message key="Hospital.2" /></h1>
                    <hr class="about-hr">
                    <h2 class="about-h2"><fmt:message key="About1" /></h2>
            <p class="about-p"> <fmt:message key="About2" /></p>
            <ol class="about-ol">
                <li class="about-li"><fmt:message key="About3" /></li>
                <ul class="about-ul">
                    <li class="about-li"><fmt:message key="About4" /></li>
                    <li class="about-li"><fmt:message key="About5" /></li>
                </ul>
                <li class="about-li"><fmt:message key="About6" /></li>
                <ul class="about-ul">
                    <li class="about-li"><fmt:message key="About7" /></li>
                    <li class="about-li"><fmt:message key="About8" /></li>
                    <li class="about-li"><fmt:message key="About9" /></li>
                </ul>
            </ol>
                    <h2 class="about1"><fmt:message key="About10" /></h2>
                    <h2 class="about1"><fmt:message key="About11" /></h2>
                    <h2 class="about1"><fmt:message key="About12" /></h2>
                    <h2 class="about1"><fmt:message key="About13" /> </h2>
                    <h2 class="about1"><fmt:message key="About14" /></h2>
        </div>
    </main>

    <div class="contentt"></div>
    <footer>
        <jsp:include page="footer.jsp" />
    </footer>

</body>


</html>