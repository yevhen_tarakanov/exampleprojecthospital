﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />





<footer class=" text-center text-muted py-5">

    <div class="container">
        <p class="mb-1 fw-bold"><fmt:message key="Example.for..." /> ©</p>
        <p class="mb-3 mt-2 text-muted fw-bold">© 2022</p>
    </div>
    <div class="footer-session">
        <h5 class="footer-session-h"><strong>session :</strong></h5>
        <h5 class="footer-session-h">${sessionScope}</h5>
    </div>

</footer>



