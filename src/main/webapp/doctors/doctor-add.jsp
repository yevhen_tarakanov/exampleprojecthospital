<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />


<!DOCTYPE HTML>
<html lang="${language}">
<head>
    <title><fmt:message key="Doctors" /></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">--%>
    <link rel="stylesheet" href="../CSS/main.css">
    <link rel="stylesheet" href="../CSS/mainMe.css">

    <link rel="stylesheet" href="/CSS/theme.css"  />
    <style id="inverter" media="none">
        html {
            background-color: #eee;
            filter: invert(100%)
        }
        .ukraine:not([src*=".svg"]),
        [style*="url("] {
            filter: invert(100%);
        }
    </style>
</head>
<body>
<header>
    <jsp:include page="/header.jsp" />
</header>

<div class="container mt-5 mb-5 text-center">
    <h1><fmt:message key="Doctor.post.additions" /></h1>
    <form action="/doctor/add" method="post">
                    <p><fmt:message key="Choose" /> :</p>
                    <div class="text-center">
                        <input type="radio" name="role_name" value="Лікар"><fmt:message key="Doctors" />
                    </div>
                    <br>

                    <input type="text" name="positions" placeholder="Посада" class="form-control"><br>

        <button type="submit" class="btn btn-success"><fmt:message key="Add.doctor" /></button>
    </form>


</div>


        <footer>
           <jsp:include page="/footer.jsp" />
        </footer>
</body>
</html>