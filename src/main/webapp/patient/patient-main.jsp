<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:useBean id="userlist" scope="request" type="java.util.List"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />


<script src="js/sortTable.js"></script>
<script src="js/tableSearch.js"></script>
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title> <fmt:message key="List.of.patients" /></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="CSS/main.css">
    <link rel="stylesheet" href="CSS/mainMe.css">

    <link rel="stylesheet" href="CSS/theme.css"  />
    <style id="inverter" media="none">
        html {
            background-color: #eee;
            filter: invert(100%)
        }
        .ukraine:not([src*=".svg"]),
        [style*="url("] {
            filter: invert(100%);
        }
    </style>
</head>
<body>
<header>
    <jsp:include page="/header.jsp" />
</header>

<main>
<div class="container mt-5">
    <h1><fmt:message key="List.of.patients" /> : </h1>
    <p><strong><fmt:message key="Click.headings" />.</strong></p>
    <p><fmt:message key="grow.A.to.Z" />.</p>
    <p><fmt:message key="decline.A.to.Z" />:</p>
</div>

    <div class="search">
        <span><fmt:message key="Search" />:</span>
        <input class="search-input"  type="text" placeholder="<fmt:message key="SearchAll" />" id="search-text" onkeyup="tableSearch()">
    </div>

<table id="userTable">
    <tr>
        <th onclick="sortTable(0)"><fmt:message key="id" /> &#8595;&#8593;</th>
        <th onclick="sortTable(1)"><fmt:message key="Lastname" /> &#8595;&#8593;</th>
        <th onclick="sortTable(2)"><fmt:message key="Firstname" /> &#8595;&#8593;</th>
        <th onclick="sortTable(3)"><fmt:message key="Patronymic" /> &#8595;&#8593;</th>
        <th onclick="sortTable(4)"><fmt:message key="Birthday" /> &#8595;&#8593;</th>
        <th onclick="sortTable(5)"><fmt:message key="Login" /> &#8595;&#8593;</th>
        <th onclick="sortTable(6)"><fmt:message key="Gender" /> &#8595;&#8593;</th>
        <th onclick="sortTable(7)"><fmt:message key="Role.name" /> &#8595;&#8593;</th>
    </tr>
    <c:forEach items="${userlist}" var="usr" varStatus="idx">

        <tr>
            <td class="numbers">${usr.id}</td>
            <td>${usr.lastname}</td>
            <td>${usr.firstname}</td>
            <td>${usr.patronymic}</td>
            <td>${usr.birthday}</td>
            <td>${usr.login}</td>
            <td>${usr.gender}</td>
            <td>${usr.role_name}</td>



            <td><a class="button-table" href="UserHandler?action=editform&id=${usr.id}" ><fmt:message key="Update" /></a></td>
            <td><a class="button-table" href="UserHandler?action=delete&id=${usr.id}" ><fmt:message key="Remove" /></a></td>

        </tr>
    </c:forEach>
</table>


</main>


            <footer>
                <jsp:include page="/footer.jsp" />
            </footer>


</body>
</html>
