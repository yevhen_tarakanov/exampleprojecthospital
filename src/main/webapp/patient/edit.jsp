<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="User" scope="request" type="com.example.hospital.model.User"/>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />


<!DOCTYPE html>
<html lang="${language}">
<head>
    <title><fmt:message key="Edit.User" /></title>
    <link rel="stylesheet" href="../CSS/main.css">
    <link rel="stylesheet" href="../CSS/mainMe.css">

    <link rel="stylesheet" href="../CSS/theme.css"  />
    <style id="inverter" media="none">
        html {
            background-color: #eee;
            filter: invert(100%)
        }
        .ukraine:not([src*=".svg"]),
        [style*="url("] {
            filter: invert(100%);
        }
    </style>
</head>
<body>
<header>
    <jsp:include page="/header.jsp" />
</header>


<h1 class="user"><fmt:message key="Users" /></h1>

<div class="container mt-sm-5 mb-sm-5">
    <div class="text-center">
<form method="POST" action='UserHandler' name="frmEditUser">
    <input
            type="hidden" name="action" value="edit"/>

    <table>
        <tr>
            <td><fmt:message key="User.ID" /></td>
            <td>
                <label>
                    <input style="background-color: #d4d4d4" type="text" name="id" readonly="readonly"
                           value= ${User.id}>

                </label>
            </td>
        </tr>
        <tr>
            <td><fmt:message key="Login" /></td>
            <td><label>
                <input type="text" name="login" value= ${User.login}>
            </label></td>
        </tr>
        <tr>
            <td><fmt:message key="Password" /></td>
            <td><label>
                <input type="text" name="password" value= ${User.password}>

            </label></td>
        </tr>
        <tr>
            <td><fmt:message key="Firstname" /></td>
            <td><label>
                <input type="text" name="firstname" value= ${User.lastname}>
            </label></td>
        </tr>
        <tr>
            <td><fmt:message key="Lastname" /></td>
            <td><label>
                <input type="text" name="lastname" value=${User.firstname}>
            </label></td>
        </tr>
        <tr>
            <td><fmt:message key="Patronymic" /></td>
            <td><label>
                <input type="text" name="patronymic" value=${User.patronymic}>
            </label></td>
        </tr>
        <tr>
            <td><fmt:message key="Gender" /></td>
            <td><label>
                <input type="text" name="gender" value=${User.gender}>
             </label></td>
        </tr>
        <tr>

            <label>
                <td><label for="start"><fmt:message key="Birthday" /> : </label></td>
             <td><input type="Date" id="start" name="birthday"
                       value="${User.birthday}"
                       min="1950-01-01" max="2022-12-31">
            </label></td>
        </tr>
        <tr>
            <td></td>
            <td><input class="button-update" type="submit" value="<fmt:message key="Update"/>"/></td>


        </tr>
    </table>
</form>
    </div>
</div>
    <footer>
        <jsp:include page="/footer.jsp" />
    </footer>
</body>
</html>